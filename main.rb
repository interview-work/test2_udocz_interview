require 'csv'

class Game
  attr_reader :board, :count

  def initialize(filename)
    @board = Board.new(filename)
    @board.load_csv(filename)
    @visited = @board.fill(false) 
  end

  def dfs(pos_row, pos_col)
    unless pos_row < 0 || pos_row >= @board.row || pos_col < 0 || pos_col >= @board.col
      if @board.blocks[pos_row][pos_col] == "1" and @visited[pos_row][pos_col] == false
        @visited[pos_row][pos_col] = true
        dfs(pos_row - 1, pos_col)
        dfs(pos_row + 1, pos_col)
        dfs(pos_row, pos_col-1)
        dfs(pos_row, pos_col + 1)
      end
    end
  end

  def bfs(pos_row, pos_col)
    queue = []
    queue << {pos_row: pos_row, pos_col: pos_col}
    @visited[queue[0][:pos_row]][queue[0][:pos_col]] = true
    until(queue.empty?)
      actual = queue.shift
      puts "Faltante de la cola"
      pp actual
      pp queue
      if actual[:pos_row] + 1 < @board.row
        if @board.blocks[actual[:pos_row]+1][actual[:pos_col]] == "1" and @visited[actual[:pos_row]+1][actual[:pos_col]] == false
          puts "Entro en 1- #{actual[:pos_row]+1}, #{actual[:pos_col]} "
          @visited[actual[:pos_row]+1][actual[:pos_col]] = true
          queue << {pos_row: actual[:pos_row]+1, pos_col: actual[:pos_col]}
        end
      end
      if actual[:pos_row] - 1 > 0
        if @board.blocks[actual[:pos_row]-1][actual[:pos_col]] == "1" and @visited[actual[:pos_row]-1][actual[:pos_col]] == false
          puts "Entro en 2- #{actual[:pos_row]-1}, #{actual[:pos_col]} "
          @visited[actual[:pos_row]-1][actual[:pos_col]] = true
          queue << {pos_row: actual[:pos_row]-1, pos_col: actual[:pos_col]}
        end
      end
      if actual[:pos_col] + 1 < @board.col
        if @board.blocks[actual[:pos_row]][actual[:pos_col]+1] == "1" and @visited[actual[:pos_row]][actual[:pos_col]+1] == false
          puts "Entro en 3- #{actual[:pos_row]}, #{actual[:pos_col]+1} "
          @visited[actual[:pos_row]][actual[:pos_col]+1] = true
          queue << {pos_row: actual[:pos_row], pos_col: actual[:pos_col]+1}
        end
      end
      if actual[:pos_col] - 1 > 0
        if @board.blocks[actual[:pos_row]][actual[:pos_col]-1] == "1" and @visited[actual[:pos_row]][actual[:pos_col]-1] == false
          puts "Entro en 4- #{actual[:pos_row]+1}, #{actual[:pos_col]-1} "
          @visited[actual[:pos_row]][actual[:pos_col]-1] = true
          queue << {pos_row: actual[:pos_row], pos_col: actual[:pos_col]-1}
        end
      end
      puts "Imprimiendo vecinos"
      pp queue
      #sleep(5)
    end  
  end
  
  def count_island(config=:dfs)
    count = 0
    pp @board.blocks
    (0..(@board.row-1)).each do |i|
      (0..(@board.col-1)).each do |j|
        if @board.blocks[i][j] == "1" and @visited[i][j] == false
          puts "Empiezo a buscar conexion #{i}, #{j}"
          count += 1
          if config == :dfs
            dfs(i, j)
          else
            bfs(i,j)
          end
        end
      end
    end
    count
  end
end


class Board
  
  attr_reader :blocks, :row, :col
  
  def initialize(row=nil, col=nil)
    @blocks = []
    @row = row
    @col = col
  end

  def load_csv(filename)
    @blocks = CSV.read(filename)
    @row = @blocks.size 
    @col = @blocks[0].size
  end

  def fill(flag)
    m_fill = Array.new(4){Array.new(5, flag)}
  end
end


game = Game.new("board.csv")
puts "El numero de islas es #{game.count_island(:bfs)}" 